var app = angular.module("EmpPayroll", ['ngRoute', 'ngStorage']);

// -----------------------Routing----------------------

app.config(["$routeProvider", function($routeProvider) {
    $routeProvider.
        when("/Signin", {
            templateUrl: "Component/Signin/Signin.html",
            controller: "signinCtrl"
        }).
        when("/Signup", {
            templateUrl: "Component/Signup/Signup.html",
        }).
        when("/Dashboard", {
            templateUrl: "Component/Dashboard/Dashboard.html",
            controller: "dashboardCtrl"
        }).
        when("/Form", {
            templateUrl: "Component/Form/Form.html",
            controller: "formCtrl"
        }).
        otherwise({
            redirectTo: "/Signin"
        });
}]);
