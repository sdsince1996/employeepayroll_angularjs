app.controller("dashboardCtrl", function ($scope,$http,$localStorage,$location, $window){
    $scope.editView = [0]
    $scope.exportView = [0];
    $scope.empid = 0
    $scope.getAllEmp = function () {
        let headersConfig = {
            headers: {
                Authorization: "Bearer " + localStorage.getItem("token")
            }
        }
        $http.get("https://localhost:7114/api/Emp/Get", headersConfig)
            .then(function (response) {
                console.log(response);
                $scope.emps = response.data.data
            }, function (error) {
                console.log(error);
            })
            

    };

    $scope.setView = function () {
        if ($scope.exportView.includes(0))
        {
            $scope.exportView = [1]
        }
        else
        {
            $scope.exportView = [0]
        }
        
    }
    $scope.export = function () {
        let headersConfig = {
            headers: {
                Authorization: "Bearer " + localStorage.getItem("token")
            }
        }
        $http.get("https://localhost:7114/api/Emp/Export", headersConfig)
            .then(function (response) {
                console.log(response);
                alert("file exported to C:/Projects/Employee_Payroll ")
            }, function (error) {
                console.log(error);
            })
    };

    $scope.exportPdf = function(){
        html2canvas(document.getElementById('table-display'), {
            onrendered: function (canvas) {
                var data = canvas.toDataURL();
             
                var docDefinition = {
                    content: [{
                        image: data,
                        width: 500
                    }]
                };
                pdfMake.createPdf(docDefinition).download("Result_"+new Date()+".pdf");
            }
        });
    }
    $scope.exportDoc = function(){
        var html = angular.element(document.querySelector('#table-display'))[0].innerHTML;
            var header = "<html xmlns:o='urn:schemas-microsoft-com:office:office' " +
                        "xmlns:w='urn:schemas-microsoft-com:office:word' " +
                        "xmlns='http://www.w3.org/TR/REC-html40'>" +
                        "<head><meta charset='utf-8'><title>Export Table to Word</title></head><body>";
            var footer = "</body></html>";
            var sourceHTML = header + "<table border='1' cellpadding='1' cellspacing='1'>" + html + "</table>" + footer;
            if (navigator.msSaveBlob) { // IE 10+
                navigator.msSaveBlob(new Blob([sourceHTML], { type: 'application/vnd.ms-word' }), "Employee.doc");
            } else {
                var source = 'data:application/vnd.ms-word;charset=utf-8,' + encodeURIComponent(sourceHTML);
                var fileDownload = document.createElement("a");
                document.body.appendChild(fileDownload);
                fileDownload.href = source;
                fileDownload.download = 'Result_'+new Date()+'.doc';
                fileDownload.click();
                document.body.removeChild(fileDownload);
            }
        }
    

    $scope.remove = function (id) {
        
        let headersConfig = {
            headers: {
                Authorization: "Bearer " + localStorage.getItem("token")
            }
        }
        $http.delete(`https://localhost:7114/api/Emp/Delete?empid=${id}`,headersConfig)
        .then(function (response) {
            console.log(response);
            alert("Record deleted successfuly");
            $scope.getAllEmp();
        }, function (error) {
            console.log(error);
            alert(error);
        })
    };

    $scope.edit = function (id, name, dept, salary, startdate){
        $scope.editView = [1]
        $scope.empid = id
        $scope.name = name
        $scope.dept = dept
        $scope.salary = salary
        $scope.startdate = startdate
    };

    $scope.update = function (name, dept, salary, startdate) {
        var data = {
            id: $scope.empid,
            name: name,
            salary: salary,
            startdate: startdate,
            dept: dept
        }
        let headersConfig = {
            headers: {
                Authorization: "Bearer " + localStorage.getItem("token")
            }
        }
        console.log(data)
        $http.put("https://localhost:7114/api/Emp/Update", JSON.stringify(data), headersConfig)
            .then(function (response) {
                console.log(response)
                $scope.getAllEmp()
            }, function (error) {
                console.log(error)
            })
            $scope.editView = [0];
    };
    $scope.cancel = function () {
        $scope.editView = [0]
    }
})