app.controller("formCtrl", function ($scope, $http, $localStorage, $location, $window) {
    $scope.save = function (name, dept, salary, startdate) {
        var data = {
            name: name,
            salary: salary,
            startdate: startdate,
            dept: dept
        }
        let headersConfig = {
            headers: {
                Authorization: "Bearer " + localStorage.getItem("token")
            }
        }
        $http.post("https://localhost:7114/api/Emp/Add", JSON.stringify(data), headersConfig)
            .then(function (response) {
                console.log(response);
                if (response.data) {
                    $scope.msg = "Post Data Submitted";
                    $location.path('/Dashboard');
                }
            }, function (error) {
                console.log(error)
            })
    };
    $scope.cancel = function() {
        $location.path('/Dashboard');
    }
})