app.controller("signupCtrl", function ($scope, $http,$localStorage,$location, $window) {
    $scope.postdata = function (firstName,  email, phone, password, confirmPassword) {
        if (password == confirmPassword) {
            var data = {
                fullName: firstName,
                emailID: email,
                password: password,
                phone: phone
            }
            $http.post("https://localhost:7114/api/User/register", JSON.stringify(data))
                .then(function (response) {
                    console.log(response);
                    if (response.data) {
                        $scope.msg = "Post Data Submitted";
                        $location.path('/Signin');
                    }
                }, function (error) {
                    console.log(error)
                })
        }
        else {
            alert("Password does not match.")
        }
    };
})